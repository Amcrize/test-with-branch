package nl.fhict.its3;

public class Player extends User {


    private String emailAddress;


    public Player(String firstName, String lastName, String username, String password, String emailAddress) {
        super(firstName, lastName, username, password);
        this.emailAddress = emailAddress;
    }


    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
